import pytest

from listerical_api.db.database import connect, disconnect


@pytest.mark.slow_integration_test
def test_database_connect_disconnect(mariadb_uri: str) -> None:
    connect(mariadb_uri)
    disconnect()
