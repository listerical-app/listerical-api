
.SILENT: clean

NAME := listerical-api

.PHONY: start
start:
	uvicorn listerical_api.main:app --reload --host 0.0.0.0

.PHONY: db-up
db-up:
	docker run \
		-d \
		--name listerical-db \
		-e MYSQL_USER=user \
		-e MYSQL_PASSWORD=pass \
		-e MYSQL_DATABASE=listerical \
		-e MYSQL_ROOT_PASSWORD=root \
		-p 13306:3306 \
		mariadb:10.5.6

.PHONY: db-down
db-down:
	docker rm -f listerical-db

.PHONY: deps
deps:
	poetry install

.PHONY: deps-no-dev
deps-no-dev:
	poetry install --no-dev

.PHONY: lint
lint:
	black .
	flake8

.PHONY: lint-check
lint-check:
	flake8
	black --check --diff .

.PHONY: test
test:
	pytest

.PHONY: unit-test
unit-test:
	pytest --without-integration

.PHONY: fast-test
fast-test:
	pytest --without-slow-integration

.PHONY: build
build:
	python setup.py sdist bdist_wheel

.PHONY: install
install:
	pip install .

.PHONY: uninstall
uninstall:
	pip uninstall $(NAME)

.PHONY: clean
clean:
	rm -rf build/ dist/ *.egg-info/ .eggs/ .pytest_cache/ .mypy_cache .coverage *.spec test-result.html htmlcov/
	find . -type d -name __pycache__ -exec rm -rf '{}' +
