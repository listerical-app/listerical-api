import sys

from loguru import logger
from pydantic import BaseSettings, ValidationError


class AppSettings(BaseSettings):
    db_uri: str
    db_max_connections: int = 100

    class Config:
        env_prefix = "listerical_"


def parse_config() -> AppSettings:
    try:
        return AppSettings()
    except ValidationError as e:
        err_msg = "\n\t".join(f"'{err['loc'][0]}' -> {err['msg']}" for err in e.errors())
        logger.error("Invalid configuration:\n\t{}", err_msg)
        sys.exit(1)
