from sqlalchemy import Column, DateTime, Integer

from listerical_api.db.database import Base


class HealthCheck(Base):
    __tablename__ = "health_checks"

    id = Column(Integer, primary_key=True)
    time = Column(DateTime)
