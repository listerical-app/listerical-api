from datetime import date
from typing import Optional

from loguru import logger
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from sqlalchemy.orm.session import Session

from .models import Menu as MenuORM
from .schemas import Menu


def get_menu_by_date(session: Session, menu_date: date) -> Optional[Menu]:
    try:
        menu_orm = session.query(MenuORM).filter(MenuORM.date == menu_date).one()
        return Menu.from_orm(menu_orm)
    except NoResultFound:
        pass
    except MultipleResultsFound:
        logger.warning("Found multiple menus for {}", menu_date)

    return None
