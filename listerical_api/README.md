# Listerical API

## Directory Layout

### `core` directory

Contains infrastructure related implementations such as configuration, authentication, etc.

### `db` directory

Implements database connection and session management.  
Exposes startup/shutdown events as well as DB model base class and session management mechanism.

### Services

API services are grouped in directories by domain, so for example,
health checks related implementation will go to `health` directory.  
Every service keeps the following structure:

`service_name/`  
├─ `__init__.py` exposes API Router named `router` with API definitions  
├─ `api.py` holds API routes declaration and logic  
├─ `models.py` declares database models to be used by the service  
└─ `schemas.py` declares Pydantic models to be used by the service

Right now services are located in a flat way rather than grouped under `services` directory to keep hierarchy simple.  
This layout might change in the future depending on growth in complexity.
