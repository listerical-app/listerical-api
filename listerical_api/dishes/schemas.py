from pydantic import BaseModel, PositiveInt, constr

from .models import FoodType


class Dish(BaseModel):
    name: constr(max_length=50)
    description: constr(max_length=200)
    image: constr(max_length=200)
    calories: PositiveInt
    food_type: FoodType

    class Config:
        orm_mode = True
