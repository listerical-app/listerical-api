from datetime import date

from fastapi.testclient import TestClient
from pytest_mock import MockerFixture

from listerical_api.menus.schemas import Dish, Meal, Menu


def test_get_menu_not_found(client: TestClient, mocker: MockerFixture) -> None:
    mocker.patch("listerical_api.menus.api.get_menu_by_date").return_value = None
    response = client.get("/api/menus/2020-09-27")

    assert response.status_code == 404
    assert response.json() == {"detail": "Menu not found"}


def test_get_menu_success(client: TestClient, mocker: MockerFixture) -> None:
    expected = Menu(
        date=date.fromisoformat("2020-10-24"),
        meals=[
            Meal(
                name="Some meal",
                opening_hours="All day long!",
                dishes=[
                    Dish(
                        name="Some dish I guess",
                        description="You should definitely eat it!",
                        image="https://img.delicious.com.au/WqbvXLhs/del/2016/06/more-the-merrier-31380-2.jpg",
                        calories=100,
                        food_type="dairy",
                    )
                ],
            )
        ],
    )

    mocker.patch("listerical_api.menus.api.get_menu_by_date").return_value = expected
    response = client.get("/api/menus/2020-09-27")

    assert response.status_code == 200
    assert Menu(**response.json()) == expected
