from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from listerical_api import dishes, health, menus
from listerical_api.db.events import db_shutdown, db_startup

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.add_event_handler("startup", db_startup)
app.add_event_handler("shutdown", db_shutdown)

app.include_router(dishes.router, prefix="/api/dishes")
app.include_router(health.router, prefix="/api/health")
app.include_router(menus.router, prefix="/api/menus")
