# flake8: noqa T400, T484  # mypy doesn't handle well with constr

from datetime import date
from typing import List

from pydantic import BaseModel, constr

from listerical_api.dishes.schemas import Dish


class Meal(BaseModel):
    name: constr(max_length=50)
    opening_hours: constr(max_length=50)
    dishes: List[Dish]

    class Config:
        orm_mode = True


class Menu(BaseModel):
    date: date
    meals: List[Meal]

    class Config:
        orm_mode = True
