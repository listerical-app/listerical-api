import pytest

from _pytest.monkeypatch import MonkeyPatch

from listerical_api.db.events import db_startup


def test_db_startup_no_db_uri_fails(monkeypatch: MonkeyPatch) -> None:
    monkeypatch.setattr("listerical_api.db.events.settings.db_uri", None)
    with pytest.raises(ValueError):
        db_startup()
