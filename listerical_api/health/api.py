from datetime import datetime

from fastapi import APIRouter, Depends
from sqlalchemy.orm.session import Session

from listerical_api.db.database import get_session

from .models import HealthCheck
from .schemas import HealthCheckResponse

router = APIRouter()


@router.get("", response_model=HealthCheckResponse)
def check_health(session: Session = Depends(get_session)) -> dict:
    session.merge(HealthCheck(id=1, time=datetime.now()))
    session.commit()
    return {"status": "ok"}
