from dotenv import load_dotenv

from ._config import parse_config

load_dotenv()
settings = parse_config()
