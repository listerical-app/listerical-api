import pytest

from sqlalchemy.orm.session import Session

from listerical_api.dishes.models import Dish
from listerical_api.dishes.repository import list_dishes
from listerical_api.dishes.schemas import Dish as DishSchema


@pytest.mark.integration_test
def test_list_dishes_multiple_succeeds(db_session: Session) -> None:
    test_dishes = [
        Dish(
            name="Pasta",
            description="Yummy.",
            image="https://somewhere.on.the/web.png",
            calories=400,
            food_type="dairy",
        ),
        Dish(
            name="Cake",
            description="Delicious!",
            image="https://somewhere.else.on.the/web.png",
            calories=500,
            food_type="dairy",
        ),
    ]
    expected = [DishSchema.from_orm(dish) for dish in test_dishes]
    db_session.add_all(test_dishes)
    db_session.commit()

    assert list_dishes(db_session) == expected


@pytest.mark.integration_test
def test_list_dishes_empty_succeeds(db_session: Session) -> None:
    assert list_dishes(db_session) == []
