from typing import Iterator
import pytest

from datetime import date

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.session import Session

from listerical_api.menus.models import Dish, Meal, Menu, meal_dish_associations


@pytest.fixture(scope="function")
def menu() -> Menu:
    return Menu(
        date=date.fromisoformat("2020-10-25"),
        meals=[
            Meal(
                name="Breakfast",
                opening_hours="8:00 - 10:00",
                dishes=[
                    Dish(
                        name="Egg",
                        description="Just an egg...",
                        image="https://eggs.are/awesome.png",
                        calories=50,
                        food_type="neutral",
                    )
                ],
            )
        ],
    )


@pytest.fixture(scope="function")
def menu_session(db_session: Session, menu: Menu) -> Iterator[Session]:
    db_session.add(menu)
    db_session.commit()
    return db_session


@pytest.mark.integration_test
def test_dishes_dont_cascade_meal(menu_session: Session, menu: Menu) -> None:
    menu_session.delete(menu.meals[0].dishes[0])
    menu_session.commit()

    assert menu_session.query(Meal).count() == 1


@pytest.mark.integration_test
def test_meals_dont_cascade_dishes(menu_session: Session, menu: Menu) -> None:
    menu_session.delete(menu.meals[0])
    menu_session.commit()

    assert menu_session.query(Dish).count() == 1


@pytest.mark.integration_test
def test_meals_dont_cascade_menus(menu_session: Session, menu: Menu) -> None:
    menu_session.delete(menu.meals[0])
    menu_session.commit()

    assert menu_session.query(Menu).count() == 1


@pytest.mark.integration_test
def test_menus_cascade_meals(menu_session: Session, menu: Menu) -> None:
    menu_session.delete(menu)
    menu_session.commit()

    assert menu_session.query(Meal).count() == 0


@pytest.mark.integration_test
def test_menus_cascade_meal_association(menu_session: Session, menu: Menu) -> None:
    menu_session.delete(menu)
    menu_session.commit()

    assert menu_session.query(meal_dish_associations).count() == 0


@pytest.mark.integration_test
def test_meals_cascade_menu_association(menu_session: Session, menu: Menu) -> None:
    menu_session.delete(menu.meals[0])
    menu_session.commit()

    assert menu_session.query(meal_dish_associations).count() == 0


@pytest.mark.integration_test
def test_adding_duplicate_menu_fails(menu_session: Session) -> None:
    menu_session.add(Menu(date=date.fromisoformat("2020-10-25"), meals=[]))

    with pytest.raises(IntegrityError):
        menu_session.commit()
