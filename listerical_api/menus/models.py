from sqlalchemy import Column, Date, ForeignKey, Integer, String, Table
from sqlalchemy.orm import relationship

from listerical_api.db.database import Base
from listerical_api.dishes.models import Dish


meal_dish_associations = Table(
    "meal_dish_associations",
    Base.metadata,
    Column("meal_id", Integer, ForeignKey("meals.id")),
    Column("dish_id", Integer, ForeignKey("dishes.id")),
)


class Meal(Base):
    __tablename__ = "meals"

    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    opening_hours = Column(String(50), nullable=False)
    dishes = relationship(Dish, secondary=meal_dish_associations)
    menu_id = Column(Integer, ForeignKey("menus.id"), nullable=False)


class Menu(Base):
    __tablename__ = "menus"

    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False, index=True, unique=True)
    meals = relationship(Meal, cascade="all, delete")
